# How to install Discord on CentOS 8

- [Download .tar.gz](https://discord.com/api/download?platform=linux&format=tar.gz)
- Extract and copy the `Discord` folder inside the i.e. `discord-0.0.13` folder to `/usr/share/discord`, PRESERVING permission modes (maybe chown to root tho).
- Copy `discord.desktop` to `/usr/share/applications`
- Copy `discord.png` to `/usr/share/pixmaps`
- Install dependencies: `sudo dnf install libXScrnSaver libatomic`
- Run Discord from your application menu
  - If nothing happens (which'll happen if libXScrnSaver is missing), try running `/usr/share/discord/Discord` in a terminal to see what's going on.
- If Discord still says it's installation is corrupt (which it does if libatomic is missing), open the web console (ctrl shift I) to see any errors of missing libraries, then use `dnf whatprovides` to find the package you need to install. Then relaunch Discord.